/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

function createCallbackFunction(lipsum,cb){

const fs = require('fs');

//  1. Read the given file lipsum.txt

fs.readFile(lipsum, 'utf-8', (err, data) => {
    if (err) {
        console.log(err);
    }
    else {
        console.log(data.toString());

        // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        fs.writeFile('upperCase.txt', data.toUpperCase(), (err, data) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log("write successfully");

                fs.appendFile('filenames.txt', 'upperCase.txt' + '\n', (err) => {
                    if (err) {
                        console.log(err);
                    }
                });

                //  3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

                fs.readFile('upperCase.txt', 'utf-8', (err, data) => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        const convertToLowercae = data.toLocaleLowerCase().split('. ');

                        fs.writeFile('./lowerCase.txt', convertToLowercae.join('\n'), (err) => {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                fs.appendFile('filenames.txt', 'lowerCase.txt' + '\n', (err) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                });


                                //  4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

                                fs.readFile('./lowerCase.txt', 'utf-8', (err, data) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                    else {

                                        const sortedData = data.split('\n').sort().join('\n');

                                        fs.writeFile('./sortedData.txt', sortedData, (err) => {
                                            if (err) {
                                                console.log(err);
                                            }
                                            else {

                                                console.log("write succesfful");
                                                fs.appendFile('filenames.txt', 'sortedData.txt', (err) => {
                                                    if (err) {
                                                        console.log(err);
                                                    }
                                                    else {

                                                        // 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

                                                        fs.readFile('./filenames.txt', 'utf-8', (err, filedata) => {
                                                            if (err) {
                                                                console.log(err);
                                                            }
                                                            else {
                                                                const data = filedata.split('\n');
                                                                data.map((file) => {
                                                                    fs.unlink(`${file}`, (err) => {
                                                                        if (err) {
                                                                            console.log(err);
                                                                        }
                                                                        else {
                                                                            console.log("succesful deleted");
                                                                        }
                                                                    });
                                                                });
                                                                fs.unlink('./filenames.txt', (err) => {
                                                                    if (err) {
                                                                        console.log(err);
                                                                    }
                                                                });
                                                            }
                                                        });



                                                    }
                                                });


                                            }

                                        });

                                    }
                                });

                            }
                        });


                    }
                });


            }

        });
    }
});
}

module.exports=createCallbackFunction;