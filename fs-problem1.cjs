/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/



function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {


    const fs = require('fs');

    // const arr = [];
     const jsonFileArray = [];
    // let index = 1;


    // arr.length = randomNumberOfFiles;
    // arr.fill(10);

    fs.mkdir(absolutePathOfRandomDirectory, (err) => {
        if (err) {
            console.log(err);
        }
        else {

            console.log("directory created");

            //let filesCreated = 0;

            // arr.map((data) => {

                for(let index=1;index<=randomNumberOfFiles;index++){

                const jsonFile = `created${index}.json`

                fs.writeFile(`${absolutePathOfRandomDirectory}/${jsonFile}`, "this is good", (err) => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log("successful file created");
                        //filesCreated++;
                        jsonFileArray.push(jsonFile);
                        if (jsonFileArray.length === randomNumberOfFiles) {
                            deleteFile(absolutePathOfRandomDirectory);
                        }

                    }

                });
                //index++;
            }
            
        }

    });

    function deleteFile(absolutePathOfRandomDirectory) {

        // fs.readdir(absolutePathOfRandomDirectory, (err, data) => {
        //     if (err) {
        //         console.log(err);
        //     }
        //     else {

       // jsonFileArray.map((file) => {
        for(let index=1;index<=randomNumberOfFiles;index++){
            fs.unlink(`${absolutePathOfRandomDirectory}/${jsonFileArray[index]}`, (err) => {
                if (err) {
                    console.log("no file" + err);
                }
                else {
                    console.log("file is deleted");

                }
            });
        //});
        //}
        // });

        }
    }


}

module.exports = fsProblem1;



